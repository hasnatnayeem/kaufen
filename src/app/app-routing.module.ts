import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './body/home/home.component';
import { ShopComponent } from './body/shop/shop.component';
import { CartComponent } from './body/cart/cart.component';
import { AboutComponent } from './body/about/about.component';
import { ContactComponent } from './body/contact/contact.component';
import { ProductComponent } from './body/product/product.component';
import { LoginComponent } from './body/login/login.component';
import { RegisterComponent } from './body/register/register.component';
import { PurchaseHistoryComponent } from './body/purchase-history/purchase-history.component';
import { MyProductsComponent } from './body/my-products/my-products.component';
import { NewProductComponent } from './body/new-product/new-product.component';
import { ThanksComponent } from './body/thanks/thanks.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', redirectTo: '' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'shop', component: ShopComponent },
  { path: 'cart', component: CartComponent },
  { path: 'purchase-history', component: PurchaseHistoryComponent },
  { path: 'my-products', component: MyProductsComponent },
  { path: 'new-product', component: NewProductComponent },
  { path: 'order-placed', component: ThanksComponent },
  { path: 'about', component: AboutComponent },
  { path: 'contact', component: ContactComponent },

  { path: 'product/:id', component: ProductComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled',
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
