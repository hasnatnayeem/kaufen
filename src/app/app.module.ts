import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from './api.service';
import { ShoppingCartService } from './shopping-cart.service';
import { LocalStorageService } from './storage.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './auth.service';

import { TokenInterceptor } from './token.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ModalComponent } from './modal/modal.component';
import { HomeComponent } from './body/home/home.component';
import { AboutComponent } from './body/about/about.component';
import { CartComponent } from './body/cart/cart.component';
import { CheckoutComponent } from './body/checkout/checkout.component';
import { ContactComponent } from './body/contact/contact.component';
import { ThanksComponent } from './body/thanks/thanks.component';
import { ProductComponent } from './body/product/product.component';
import { ShopComponent } from './body/shop/shop.component';
import { LoginComponent } from './body/login/login.component';
import { RegisterComponent } from './body/register/register.component';
import { PurchaseHistoryComponent } from './body/purchase-history/purchase-history.component';
import { MyProductsComponent } from './body/my-products/my-products.component';
import { NewProductComponent } from './body/new-product/new-product.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ModalComponent,
    HomeComponent,
    AboutComponent,
    CartComponent,
    CheckoutComponent,
    ContactComponent,
    ThanksComponent,
    ProductComponent,
    ShopComponent,
    LoginComponent,
    RegisterComponent,
    PurchaseHistoryComponent,
    MyProductsComponent,
    NewProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    ApiService, 
    ShoppingCartService,
    LocalStorageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
