import { Component, OnInit } from '@angular/core';
import { Subscription } from "rxjs";
import { ShoppingCartService } from '../shopping-cart.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private isAuthenticated: boolean = false;
  totalItemsInCart:number = 0;
  private cartSubscription: Subscription;
  private authSubscription: Subscription;

  constructor(
    private authService: AuthService,
    private shoppingCartService: ShoppingCartService,
    ) 
  { 
    this.isAuthenticated = this.authService.isAuthenticated();
  }

  ngOnInit() {
    this.cartSubscription = this.shoppingCartService.get().subscribe((cart) => {
      this.totalItemsInCart = cart.items.map((x) => x.quantity).reduce((p, n) => p + n, 0);
    });

    this.authSubscription = this.authService.get().subscribe((isAuthenticated) => {
      this.isAuthenticated = isAuthenticated;
    });
  }

  ngOnDestroy(): void {
    if (this.cartSubscription) {
      this.cartSubscription.unsubscribe();
    }
  }

  logout() {
    this.authService.logout();
  }

}
