import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { Observable } from "rxjs";
import { Observer } from "rxjs";

const apiUrl = "http://35.200.224.144:8090/api/v1";


@Injectable()
export class AuthService {
  private subscriptionObservable: Observable<boolean>;
  private subscribers: Array<Observer<boolean>> = new Array<Observer<boolean>>();

  constructor(
    private http: HttpClient,
    private router: Router,
    ) { 
      this.subscriptionObservable = new Observable<boolean>((observer: Observer<boolean>) => {
        this.subscribers.push(observer);
        observer.next(this.isAuthenticated());
        return () => {
          this.subscribers = this.subscribers.filter((obs) => obs !== observer);
        };
      });
    }

  public get(): Observable<boolean> {
    return this.subscriptionObservable;
  }

  public getToken(): string {
    let token = localStorage.getItem('token');
    return token ? token : "";
  }


  public getUserId(): number {
    let user:any = JSON.parse(localStorage.getItem('currentUser'));
    return user ? user.id : 0;
  }

  public isAuthenticated(): boolean {
    const token = this.getToken();
    if (token) {
      return true;
    }
    return false;
  }

  login(email: string, password: string) {
    let formData: FormData = new FormData(); 
    formData.append('emailId', email); 
    formData.append('password', password); 

    return this.http.post<any>(`${apiUrl}/login`, formData)
      .pipe(map(response => {
        if (response.user && response.user.token) {
          localStorage.setItem('currentUser', JSON.stringify(response.user));
          localStorage.setItem('token', response.user.token);
        }
        this.dispatch(this.isAuthenticated());
        return response.user;
      }));
  }


  register(data) {
    data.role = "ROLE_USER";
    return this.http.post<any>(`${apiUrl}/user`, data)
      .pipe(map(response => {
        
        console.log(response);
        return response;
      }));
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.dispatch(this.isAuthenticated());
    this.router.navigate(['/']);
  }

  private dispatch(isAuthenticated): void {
    this.subscribers
        .forEach((sub) => {
          try {
            sub.next(isAuthenticated);
          } catch (e) {
          }
        });
  }

}