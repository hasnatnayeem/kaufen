import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from "@angular/core";
import { CartItem } from "../../cart-item.entity";
import { Product } from "../../product.entity";
import { ShoppingCart } from "../../shopping-cart.entity";
import { ShoppingCartService } from "../../shopping-cart.service";
import { Observable } from "rxjs";
import { Subscription } from "rxjs";
import { AuthService } from 'src/app/auth.service';
import { ApiService } from 'src/app/api.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';


interface ICartItemWithProduct extends CartItem {
  product: Product;
  totalCost: number;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

export class CartComponent implements OnInit {

  public cart: Observable<ShoppingCart>;
  public cartItems: ICartItemWithProduct[];
  public itemCount: number;
  private isAuthenticated: boolean = false;
  private cartSubscription: Subscription;
  private cartProducts;
  private buyerId;
  private loading = false;

  public constructor(
    private authService: AuthService,
    private shoppingCartService: ShoppingCartService,
    private apiService: ApiService,
    private router: Router,
  ) 
  {
    this.isAuthenticated = this.authService.isAuthenticated();
    this.buyerId = this.authService.getUserId();
  }

  public emptyCart(): void {
    this.shoppingCartService.empty();
  }

  public ngOnInit(): void {
    this.cart = this.shoppingCartService.get();
    this.cartSubscription = this.cart.subscribe((cart) => {
      this.itemCount = cart.items.map((x) => x.quantity).reduce((p, n) => p + n, 0);
      this.cartProducts = cart.items;
      this.cartItems = cart.items
        .map((item) => {
          return {
            ...item,
            totalCost: item.product.price * item.quantity
          };
        });

    });
  }
  
  removeProductFromCart(product) {
    this.shoppingCartService.addItem(product, 0);
  }

  public ngOnDestroy(): void {
    if (this.cartSubscription) {
      this.cartSubscription.unsubscribe();
    }
  }

  placeOrder() {
    if (!this.buyerId) {
      alert("An error occurred");
      return;
    }
    this.loading = true;
    let products = [];
    for (let cartProduct of this.cartProducts) {
      let product = cartProduct.product;
      product.buyerId = this.buyerId;
      product.qty = cartProduct.quantity;
      products.push(product);
    }
    this.apiService.placeOrder(products).pipe(first())
    .subscribe(
      data => {
        this.shoppingCartService.empty();
        this.loading = false;
        this.router.navigate(['/order-placed']);
      },
      error => {
        console.log(error);
      });

  }

}
