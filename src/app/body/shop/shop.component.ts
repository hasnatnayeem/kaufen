import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../api.service';
import { Product } from 'src/app/product.entity';
import { ShoppingCartService } from 'src/app/shopping-cart.service';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})


export class ShopComponent implements OnInit {
  products: Product[];
  categories: any;
  query = "";
  results: Object;
  searchTerm$ = new Subject<string>();
  searchQuery = "";

  constructor(
    private apiService: ApiService,
    private cartService: ShoppingCartService,
    private route: ActivatedRoute,
  ) {

    this.apiService.searchProduct(this.searchTerm$)
      .subscribe(results => {
        this.products = results;
        this.query = this.searchQuery;
      });

  }

  ngOnInit() {
    this.query = "";
    this.route.queryParams
      .subscribe(params => {
        if (params.category) {
          this.getFilteredProducts(params.category);
        }
        else {
          this.getProducts();
        }
        this.query = params.categoryName ? params.categoryName : "";
      });

    this.getCategories();
  }

  getProducts() {
    this.apiService.getProducts()
      .subscribe(res => {
        this.products = res;
      }, err => {
        console.log(err);
      });
  }

  getFilteredProducts(categoryId, order="ASC", sortBy="name") {
    this.apiService.getFilteredProducts(categoryId, order, sortBy)
    .subscribe(res => {
      this.products = res;
    }, err => {
      console.log(err);
    });
  }

  getCategories() {
    this.apiService.getcategories()
      .subscribe(res => {
        this.categories = res;
      }, err => {
        console.log(err);
      });
  }

  addToCart(product) {
    this.cartService.addItem(product, 1);
  }

}
