import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../api.service';
import { Product } from 'src/app/product.entity';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  products: Product[];

  constructor(private apiService: ApiService) {

  }

  ngOnInit() {
    this.apiService.getProducts()
      .subscribe(res => {
        this.products = res;
      }, err => {
        console.log(err);
      });
  }

}
