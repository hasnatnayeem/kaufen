import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { ApiService } from '../../api.service';
import { ShoppingCartService } from '../../shopping-cart.service';
import { Product } from 'src/app/product.entity';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  product: any = {};
  quantity = 1;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService,
    private cartService: ShoppingCartService,
  ) { 

  }

  ngOnInit() {
    this.getProductDetails(this.route.snapshot.params['id']);
  }

  getProductDetails(id) {
    this.apiService.getProduct(id)
      .subscribe(data => {
        this.product = data;
      });
  }

  addToCart() {
    this.cartService.addItem(this.product, this.quantity);
  }

}
