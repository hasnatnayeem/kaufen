import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Purchase } from 'src/app/purchase.entity';

@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.scss']
})
export class PurchaseHistoryComponent implements OnInit {
  purchases: any;

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getPurchaseHistory();
  }

  getPurchaseHistory() {
    this.apiService.getPurchaseHistory()
      .subscribe(res => {
        this.purchases = res;
      }, err => {
        console.log(err);
      });
  }

}
