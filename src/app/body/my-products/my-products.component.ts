import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/product.entity';
import { ApiService } from 'src/app/api.service';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-my-products',
  templateUrl: './my-products.component.html',
  styleUrls: ['./my-products.component.scss']
})
export class MyProductsComponent implements OnInit {
  products: Product[];
  userId:any;

  constructor(
    private apiService: ApiService,
    private authService: AuthService
    ) 
  {
      this.userId = this.authService.getUserId();

  }

  ngOnInit() {
    if (!this.userId) {
      return;
    }
    this.apiService.getProducts()
      .subscribe(res => {
        this.products = res.filter(product => product.sellerId === this.userId);
      }, err => {
        console.log(err);
      });
  }

}
