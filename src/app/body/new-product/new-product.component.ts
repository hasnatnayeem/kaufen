import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  FormBuilder, FormGroup, Validators, FormsModule,
  ReactiveFormsModule
} from '@angular/forms';
import { first } from 'rxjs/operators';
import { AuthService } from 'src/app/auth.service';
import { ApiService } from 'src/app/api.service';
import { Product } from 'src/app/product.entity';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit {
  productForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  fileToUpload: File = null;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private apiService: ApiService,
  ) { }

  ngOnInit() {
    this.productForm = this.formBuilder.group({
      productName: ['', Validators.required],
      slug: ['', Validators.required],
      description: ['', Validators.required],
      categoryId: ['', Validators.required],
      stock: ['', Validators.required],
      price: ['', Validators.required],
      type: ['', Validators.required],
      prodCondition: ['', Validators.required],
      file: ['', Validators.required],
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() { return this.productForm.controls; }

  onFileChange(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onSubmit() {
    this.submitted = true;

    if (this.productForm.invalid) {
      return;
    }

    this.loading = true;
    let product = this.productForm.value;
    delete product.file;
    console.log(JSON.stringify(product));

    // return;
    this.apiService.uploadProduct(product, this.fileToUpload)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/my-products']);
        },
        error => {
          // this.alertService.error(error);
          this.loading = false;
        });
  }

}
