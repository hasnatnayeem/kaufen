export class Purchase {

    purchaseItemId: number;
    purchaseId: number;
    productId: number;
    quantity: number;
    subtotal: number;
    itemStatus: number;
    purchaseDate: string;
    productName: string;
    price: number;

}