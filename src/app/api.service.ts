import { Injectable } from '@angular/core';

import { Product } from './product.entity';
import { Purchase } from './purchase.entity';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { ShoppingCart } from './shopping-cart.entity';


const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const apiUrl = "http://35.200.224.144:8090/api/v1";

@Injectable()
export class ApiService {
    constructor(
        private http: HttpClient,
        private authService: AuthService,
        private router: Router,
    ) {

    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            return of(result as T);
        };
    }

    getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(`${apiUrl}/product`)
            .pipe(
                map((res: any) => res.data),
                catchError(this.handleError('getProducts', []))
            );
    }

    getProduct(id): Observable<Product> {
        const url = `${apiUrl}/product/${id}`;
        return this.http.get<Product>(url).pipe(
            map((res: any) => res.data),
            catchError(this.handleError<Product>(`getProduct id=${id}`))
        );
    }

    addProduct(product): Observable<Product> {
        return this.http.post<Product>(`${apiUrl}/product`, product, httpOptions).pipe(
            tap((product: Product) => console.log(`added product w/ id=${product.id}`)),
            catchError(this.handleError<Product>('addProduct'))
        );
    }

    updateProduct(id, product): Observable<any> {
        const url = `${apiUrl}/product/${id}`;
        return this.http.put(url, product, httpOptions).pipe(
            tap(_ => console.log(`updated product id=${id}`)),
            catchError(this.handleError<any>('updateProduct'))
        );
    }

    deleteProduct(id): Observable<Product> {
        const url = `${apiUrl}/product${id}`;

        return this.http.delete<Product>(url, httpOptions).pipe(
            tap(_ => console.log(`deleted product id=${id}`)),
            catchError(this.handleError<Product>('deleteProduct'))
        );
    }

    getcategories(): Observable<any> {
        return this.http.get(`${apiUrl}/categories/count`)
            .pipe(
                map((res: any) => res.data),
                catchError(this.handleError('getProducts', []))
            );
    }

    getFilteredProducts(categoryId, order, sortBy): Observable<any> {
        return this.http.get(`${apiUrl}/product/filter?category=${categoryId}&order=${order}&sortBy=${sortBy}`)
            .pipe(
                map((res: any) => res.data),
                catchError(this.handleError('getProducts', []))
            );
    }

    searchProduct(terms: Observable<string>) {
        return terms.pipe(debounceTime(400)
            , distinctUntilChanged()
            , switchMap(term => this.searchEntries(term)));
    }

    searchEntries(term) {
        return this.http.get(`${apiUrl}/product/search?query=${term}`)
            .pipe(
                map((res: any) => res.data),
                catchError(this.handleError('searchEntries', []))
            );
    }

    getPurchaseHistory(): Observable<Purchase> {
        let userId = this.authService.getUserId();
        if (!userId) {
            return;
        }
        const url = `${apiUrl}/product/bought/${userId}`;
        return this.http.get<Purchase>(url).pipe(
            map((res: any) => res.data),
            catchError(this.handleError<Product>(`getPurchaseHistory userId=${userId}`))
        );
    }

    uploadProduct(product: Product, file: File) {
        let formData: FormData = new FormData();
        formData.append('product', JSON.stringify(product));
        formData.append('file', file);

        return this.http.post<any>(`${apiUrl}/product`, formData)
            .pipe(map(response => {
                this.router.navigate(['/my-products'])
            }));
    }

    placeOrder(cart) {
        return this.http.post<any>(`${apiUrl}/product/buy`, cart)
            .pipe(map(response => response));
    }


}