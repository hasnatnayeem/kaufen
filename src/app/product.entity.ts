export class Product {
    id: number;
    productName: string;
    slug: string;
    description: string;
    price: number;
    categoryId: number;
    categoryName: string;
    type: number;
    height: number;
    length: number;
    depth: number;
    weight: number;
    prodCondition: number;
    sellerId: number;
    stock: number;
    publicationDate: string;
    imgPath: string;
    itemStatus: number;
}


